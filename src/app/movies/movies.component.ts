import { Component, OnInit } from '@angular/core';
/*import {MatCardModule} from '@angular/material/card';
import { MovieComponent } from '../movie/movie.component';*/
import {AngularFireDatabase} from '@angular/fire/database'

/*export interface Tile {
  color: string;
  cols: number;
  rows: number;
  title: string;
  id: number;
  studio: string;
  weekendIncome:string;
}*/


@Component({
  selector: 'movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {
  name = "no movie";
  studio = "";
  displayedColumns: string[] = ['id', 'title', 'studio', 'income', 'delete'];

  movies = [];
  studios = [];

  toDelete(element)
  {

    let start, end = this.movies;
    let id = element.id;
    let i = 0;
    let deleted = [];
      
    for(let movie of this.movies)
    {
      deleted[i] = movie.id;
      i++;
    }
    
    start = this.movies.slice(0,deleted.indexOf(id));
    end = this.movies.slice(deleted.indexOf(id)+1, this.movies.length+1);
    this.movies = start;
    this.movies = this.movies.concat(end);   
    this.name = element.title;

  }
 
  /*movies: Tile[]= [
    {cols: 1, rows: 1, color: 'lightblue','id': 1, 'title': 'Spider-Man: Into The Spider-Verse ', 'studio': 'Sony', 'weekendIncome': '$ 35,400,000' },
    {cols: 1, rows: 1, color: 'lightgreen','id': 2, 'title': 'The Mule', 'studio': 'WB', 'weekendIncome': '$ 17,210,000'},
    {cols: 1, rows: 1, color: 'lightpink','id': 3, 'title': "Dr. Seuss' The Grinch (2018)", 'studio': 'Uni', 'weekendIncome': '$ 11,580,000'},
    {cols: 1, rows: 1, color: '#DDBDF1','id': 4, 'title': 'Ralph Breaks the Internet', 'studio': 'BV', 'weekendIncome': '$ 9,589,000'},
    {cols: 1, rows: 1, color: 'lightyellow','id': 5, 'title': 'Mortal Engines', 'studio': 'Uni', 'weekendIncome': '$ 7,501,000'},
   
    
  ];*/
  
 
  constructor( private db:AngularFireDatabase ) { }

  ngOnInit() {
    this.db.list('/movies').snapshotChanges().subscribe(
      movies => {
        this.movies = [];
        this.studios = ['all'];
        movies.forEach(
          movie => {
            let y = movie.payload.toJSON();
            this.movies.push(y);
            let stu = y['studio'];
            if (this.studios.indexOf(stu) == -1) {
              this.studios.push(y['studio']);
            }
          }
        )
      }
    )
    }

    toFilter() {
      let id = 1;
      this.db.list('/movies').snapshotChanges().subscribe(
        movies => {
          this.movies = [];
          movies.forEach(
            movie => {
              let y = movie.payload.toJSON();
              if (this.studio == 'all') {
                this.movies.push(y);
              }
              else if (y['studio'] == this.studio) {
                y['id'] = id;
                id++;
                this.movies.push(y);
              }
            }
          )
        }
      )
    }
 
}
